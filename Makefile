BUNDLE = ksi.lv2
INSTALL_DIR = /usr/local/lib/lv2

$(BUNDLE): manifest.ttl ksi.ttl ksi.so ksi_gui.so ksi.png
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp $^ $(BUNDLE)

ksi.so: ksi.cpp ksi.peg
	g++ -shared -fPIC -DPIC ksi.cpp `pkg-config --cflags --libs lv2-plugin` -o ksi.so

ksi_gui.so: ksi_gui.cpp ksi.peg
	g++ -shared -fPIC -DPIC ksi_gui.cpp `pkg-config --cflags --libs lv2-gui` -o ksi_gui.so

ksi.peg: ksi.ttl
	lv2peg ksi.ttl ksi.peg

install: $(BUNDLE)
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R $(BUNDLE) $(INSTALL_DIR)

uninstall:
	rm -rf $(INSTALL_DIR)/$(BUNDLE)

clean:
	rm -rf $(BUNDLE) ksi.so ksi_gui.so ksi.peg
